#===================================
#     Configuración parámetros de simulación
#===================================

set val(chan)           Channel/WirelessChannel   ;
set val(prop)           $::env(envPropagacion)    ;# Modelo radio-propagation
set val(netif)          Phy/WirelessPhy           ;
set val(mac)            Mac/802_11                ;
set val(ll)             LL                        ;
set val(ant)            Antenna/OmniAntenna       ;
set val(ifqlen)         50                        ;
set val(nn)             $::env(nNodos)            ;# Número de Nodos
set val(rp)             $::env(envEnrutamiento)   ;# Protocolo de enrutaminto
set val(x)              $::env(topox)                       ;# Dimensiones en X de la tophografia
set val(y)              $::env(topoy)                       ;# Dimensiones en Y de la tophografia
set val(stop)           60                        ;# Tiempo en que la simulacion termina

set val(cp) "./mobilidad.tcl"                     ;# Ruta generacion de mobilidad
set val(sc) "./trafico.tcl"                ;# Ruta generacion de trafico

if {$val(rp) == "DSR"} {                   ;# Condicion para tipo interfaz de cola protocolo DSR
  set val(ifq) CMUPriQueue                 ;
} else {
  set val(ifq) Queue/DropTail/PriQueue     ;
}

#===================================
#     Configuracion Estandar IEEE 802.11b
#===================================
Mac/802_11 set SlotTime_          0.000020        ;# 20us
Mac/802_11 set SIFS_              0.000010        ;# 10us
Mac/802_11 set PreambleLength_    144             ;# 144 bit
Mac/802_11 set PLCPHeaderLength_  48              ;# 48 bits
Mac/802_11 set PLCPDataRate_      1.0e6           ;# 1Mbps
Mac/802_11 set dataRate_          11.0e6          ;# 11Mbps
Mac/802_11 set basicRate_         1.0e6           ;# 1Mbps
Mac/802_11 set RTSThreshold_      3000            ;


Phy/WirelessPhy set CPThresh_ 10.0
Phy/WirelessPhy set CSThresh_ 1.559e-11
Phy/WirelessPhy set RXThresh_ 3.652e-10           ; 
Phy/WirelessPhy set RXThresh_ 2.62861e-09         ;
Phy/WirelessPhy set bandwidth_ 2e6
Phy/WirelessPhy set Pt_ 0.28183815                ; # Potencia de transmision
Phy/WirelessPhy set freq_ 2.472e+9                ; # frecuencia 2.4 GHz 802.11b
Phy/WirelessPhy set L_ 1.0                        ;


#===================================
#     Configuración de las antenas para que estén centradas en el nodo
#     y a 1,5 metros por encima de él
#===================================
Antenna/OmniAntenna set X_ 0
Antenna/OmniAntenna set Y_ 0
Antenna/OmniAntenna set Z_ 1.5                ; # Altura de la antena
Antenna/OmniAntenna set Gt_ 1.0               ; # Ganancia de tranasmision
Antenna/OmniAntenna set Gr_ 1.0               ; # Ganancia de recepcion

#=====================================================================

# Configuracion de Arvhivos

#=====================================================================

# *** Inicializacion Simulador ***
set ns_              [new Simulator]
$ns_ use-newtrace

# *** Inicializacion archivo de Trazas  ***
set tracefd     [open tr_escenario.tr w]
$ns_ trace-all $tracefd

# *** Inicializacion Network Animator ***
set namtrace [open nam_escenario.nam w]
$ns_ namtrace-all-wireless $namtrace $val(x) $val(y)

# *** Configuracion de la topografia ***
set topo       [new Topography]
$topo load_flatgrid $val(x) $val(y)

set god_ [create-god $val(nn)]
set chan_1_ [new $val(chan)]


# Configuracion de los nodos
$ns_ node-config -adhocRouting $val(rp) \
                 -llType $val(ll) \
                 -macType $val(mac) \
                 -ifqType $val(ifq) \
                 -ifqLen $val(ifqlen) \
                 -antType $val(ant) \
                 -propType $val(prop) \
                 -phyType $val(netif) \
                 -channel $chan_1_ \
                 -topoInstance $topo \
                 -agentTrace ON \
                 -routerTrace ON \
                 -macTrace OFF \
                 -movementTrace OFF

# Creacion de nodos
for {set i 0} {$i < $val(nn) } {incr i} {
  set node_($i) [$ns_ node]

  $node_($i) set X_ 0.0
  $node_($i) set Y_ 0.0
  $node_($i) set Z_ 0.0

  $ns_ initial_node_pos $node_($i) 20
  $node_($i) random-motion 0
}


#
# Carga de modelo de movimiento en los nodos (random-way-point)
#
puts "Loading connection pattern..."
source $val(cp)
#
# Carga de trafico entre nodos (TCP-FTP)
#
puts "Loading scenario file..."
source $val(sc)

# Se temina la simulacion en el tiempo indicado
$ns_ at $val(stop) "stop"
for {set i 0} {$i < $val(nn) } {incr i} {
    $ns_ at $val(stop) "$node_($i) reset";
}
$ns_ at $val(stop).01 "puts \"NS EXITING...\" ; $ns_ halt"

proc stop {} {
        global ns_ tracefd namtrace

        # Reset Trace File
        $ns_ flush-trace

        close $tracefd
        close $namtrace
        exec nam nam_escenario.nam &
        exit 0
}

# Se inicia la simulacion
puts "Se inicia la simulacion..."
$ns_ run
