# Packet delivery Ratio   
# http://harrismare.net/2011/07/14/packet-delivery-ratio-packet-lost-end-to-end-delay/



BEGIN { 
        sendLine = 0; 
        recvLine = 0; 
        fowardLine = 0; 
        currTime = prevTime = 0
        tic = 0.1
} 

$0 ~/^s.* AGT/ { 
        sendLine ++ ; 
} 

$0 ~/^r.* AGT/ { 
        recvLine ++ ; 
} 

$0 ~/^f.* RTR/ { 
        fowardLine ++ ; 
} 

{
    	# Trace line format: normal
	if ($2 != "-t") {
		time = $2
	}
	# Trace line format: new
	if ($2 == "-t") {
		time = $3
	}
 
 	# Init prevTime to the first packet recv time
	if(prevTime == 0)
		prevTime = time

    	# plots from the output of this script
		currTime += (time - prevTime)
		if (currTime >= tic) {
			printf("%.5f %.5f\n",time,sendLine)
			recv = 0
			currTime = 0
		}
		prevTime = time
}
 
END { 
        # printf "s:%d r:%d, r/s Ratio:%.4f, f:%d \n", sendLine, recvLine, (recvLine/sendLine),fowardLine; 
}
