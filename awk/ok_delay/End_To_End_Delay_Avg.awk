BEGIN {

    seqno = -1;    

#    droppedPackets = 0; 

#    receivedPackets = 0; 

    count = 0;
    num_recv =0;
    send = 0;
}

{

    	# Trace line format: normal
	if ($2 != "-t") {
		event = $1
		time = $2
		if (event == "+" || event == "-") node_id = $3
		if (event == "r" || event == "d") node_id = $4
		flow_id = $8
		pkt_id = $12
		flow_t = $5
		level = "AGT"
        packet_type = $7
        unique_id = $6
	}
	# Trace line format: new
	if ($2 == "-t") {
		event = $1
		time = $3
		node_id = $5
		flow_id = $39
		pkt_id = $41
		flow_t = $45
		level = $19
        packet_type = $35
	}


    if(level == "AGT" && event== "s" && seqno < pkt_id) {
          seqno = pkt_id;
    } 

    #end-to-end delay

    if(level == "AGT" && sendTime[pkt_id] == 0 && (event == "+" || event == "s")) {
          start_time[pkt_id] = time;
          send++
    } else if(level == "AGT" && event == "r" ) {
        end_time[pkt_id] = time;
        num_recv++
    } else if(event== "D" && level == "AGT")  {
          end_time[pkt_id] = -1;
    } 

}

 
END {     
    for(i=0; i<=seqno; i++) {
          if(end_time[i] > 0) {
              delay[i] = end_time[i] - start_time[i];
                  count++;
        }
            else
            {
                  delay[i] = -1;
            }
    }

    min = 10000000
    max = 0

    for(i=0; i<=seqno; i++) {
          if(delay[i] > 0) {
              n_to_n_delay = n_to_n_delay + delay[i];
              if(delay[i] < min ){
                  min = delay[i]
              }
              if(delay[i] > max ){
                  max = delay[i]
              }
        }         
    }

    print "Total End-to-End Delay   = " n_to_n_delay "s";
    print "Minimal End-to-End Delay   = " min * 1000 "ms";
    print "Maximal End-to-End Delay   = " max * 1000 "ms";
    print "Average End-to-End Delay    = " (n_to_n_delay/count) * 1000 " ms";
} 
