BEGIN {

    seqno = -1;    

#    droppedPackets = 0; 

#    receivedPackets = 0; 

    count = 0;
    num_recv=0
}

{

    	# Trace line format: normal
	if ($2 != "-t") {
		event = $1
		time = $2
		if (event == "+" || event == "-") node_id = $3
		if (event == "r" || event == "d") node_id = $4
		flow_id = $8
		pkt_id = $12
		flow_t = $5
		level = "AGT"
        packet_type = $7
        unique_id = $6
	}
	# Trace line format: new
	if ($2 == "-t") {
		event = $1
		time = $3
		node_id = $5
		flow_id = $39
		pkt_id = $41
		flow_t = $45
		level = $19
        packet_type = $35
	}


    if(level == "AGT" && event== "s" && seqno < pkt_id) {
          seqno = pkt_id;
    } 

    #end-to-end delay
    if(level == "AGT" && sendTime[pkt_id] == 0 && (event == "+" || event == "s")) {
          start_time[pkt_id] = time;
    } else if(level == "AGT" && event == "r" ) {
        end_time[pkt_id] = time;
    } else if(event== "D" && level == "AGT")  {
          end_time[pkt_id] = -1;
    } 

}

 
END {    
     jitter1 = jitter2 = tmp_recv = 0
     prev_delay = -1

    for(i=0; i<=seqno; i++) {
          if(end_time[i] > 0) {
              delay[i] = end_time[i] - start_time[i];

             if(end_time[i] != 0) {
                     tmp_recv++
                  if(prev_time != 0) {
                       e2eDelay = end_time[i] - start_time[i]
                       
                       if(delay[i] < 0) delay[i] = 0
                       if(prev_delay != -1) {
                       jitter1 += abs(e2eDelay - prev_e2eDelay)
                       jitter2 += abs(delay[i]-prev_delay)
                       }

                       printf("%.5f %.5f\n",i, (e2eDelay - prev_e2eDelay))

                       prev_delay = delay[i]
                       prev_e2eDelay = e2eDelay
                  }
                  prev_time = end_time[i]
             }
        }
    }

} 

  function abs(value) {
       if (value < 0) value = 0-value
       return value
  }
