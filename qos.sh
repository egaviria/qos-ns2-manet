#!/bin/bash
#
# Inicializacion  de variables
#
nNodos=1
#
nPropagacion=0
envPropagacion=""
tempPropagacion=0
#
nEnrutamiento=0
tempEnrutamiento=0
envEnrutamiento=""
#
# Ingreso de parametros
#
echo
echo
echo "------------------------------------------------------------"
echo "Bienvenido al modulo de monitoreo de los parámetros que generan calidad de servicio (QoS)."
echo "------------------------------------------------------------"
echo
echo "Ingrese los parametros para configurar la red MANET:"
echo
read -p "Numero de Nodos: " nNodos
#
echo
echo "Ingrese la topologia(x luego y):"
read -p "X: " topox
read -p "Y: " topoy
echo
#
echo "Modelo de Propagació: "
echo "Seleccione una opción: "
echo "  1) para TwoRayGround"
echo "  2) para Shadowing"
echo "  3) para FreeSpace"
#
#
while [ $nPropagacion == 0 ]
do
    read -p "Ingrese 1, 2 ó 3: " tempPropagacion
    if [ $tempPropagacion -gt 0 ] && [ $tempPropagacion -lt 4 ]
    then
        nPropagacion=$tempPropagacion
    else
        echo "opción no valida."
    fi
done
#
case $nPropagacion in
  1) envPropagacion="Propagation/TwoRayGround";;
  2) envPropagacion="Propagation/Shadowing";;
  3) envPropagacion="Propagation/FreeSpace";;
esac
#
echo
echo "Protocolo de enrutamiento: "
echo "Seleccione una opción: "
echo "  1) para OLSR"
echo "  2) para AODV"
echo "  3) para DSR"
echo "  4) para ZRP"
echo "  5) para DSDV"
echo "  6) para TORA"
echo "  7) para PUMA"
#
while [ $nEnrutamiento == 0 ]
do
    read -p "Ingrese del 1 al 7: " tempEnrutamiento
    if [ $tempEnrutamiento -gt 0 ] && [ $tempEnrutamiento -lt 8 ]
    then
        nEnrutamiento=$tempEnrutamiento
        echo $nEnrutamiento
    else
        echo "opción no valida."
        echo
    fi
done
#
case $nEnrutamiento in
  1) envEnrutamiento="OLSR";;
  2) envEnrutamiento="AODV";;
  3) envEnrutamiento="DSR";;
  4) envEnrutamiento="ZRP";;
  5) envEnrutamiento="DSDV";;
  6) envEnrutamiento="TORA";;
  7) envEnrutamiento="PUMA";;
esac
#
#
echo
echo "------------------------------------------------------------"
echo "Configuración red MANET:"
echo "------------------------------------------------------------"
#
echo "Numero de Nodos: $nNodos"
echo "Topologia: $topox $topoy"
echo "Modelo de Propagació: $envPropagacion"
echo "Protocolo de enrutamiento: $envEnrutamiento"
echo "------------------------------------------------------------"
#
# Se exportan los datos para ser usados en la simulacion
#
export nNodos=$nNodos
export topox=$topox
export topoy=$topoy
export envPropagacion=$envPropagacion
export envEnrutamiento=$envEnrutamiento
#
# generacion modelo de mobilidad y trafico
#
~/ns-allinone-2.35/ns-2.35/indep-utils/cmu-scen-gen/setdest/setdest -v 1 -n "$((nNodos-1))" -p 0 -M 20 -t 60 -x $topox -y $topoy > mobilidad.tcl
ns cbrgen.tcl -type tcp -nn "$((nNodos-1))" -seed 0.0 -mc "$((nNodos/2))" > trafico.tcl
#
# Inicio Simulacion
#
ns escenario.tcl
#
echo "------------------------------------------------------------"
echo "Fin de la Simulación"
echo "------------------------------------------------------------"
#
# Uso de Filtros AWK y graficas
#
echo ""
echo "PDR:"
gawk -f ./awk/ok_PDR/Packet_Delivery_Ratio.awk tr_escenario.tr
gawk -f ./awk/ok_PDR/Packet_Delivery_Ratio_Send.awk tr_escenario.tr > PDR_Send.data
gawk -f ./awk/ok_PDR/Packet_Delivery_Ratio_Recived.awk tr_escenario.tr > PDR_Rec.data
gawk -f ./awk/ok_PDR/Packet_Delivery_Ratio_Lost.awk tr_escenario.tr > PDR_Lost.data
xgraph -bb -t Packet_Delivery_Ratio -x "time (s)" -y packets -bg white PDR_Send.data PDR_Rec.data PDR_Lost.data -geometry 800x400
gawk -f ./awk/ok_PDR/pdr_inst.awk tr_escenario.tr > PDR_Inst.data
xgraph -bb -t Packet_Delivery_Ratio -x "time (s)" -y packets -bg white PDR_Inst.data -geometry 800x400
echo ""
echo "Throughput:"
gawk -f ./awk/ok_throughput/Throughput_Avg.awk tr_escenario.tr
gawk -f ./awk/ok_throughput/Throughput_Inst.awk tr_escenario.tr > Throughput.data
xgraph -bb -t Throughput  -x "time (s)" -y kbps -bg white Throughput.data -geometry 800x400
echo ""
echo "e2e Delay:"
gawk -f ./awk/ok_delay/End_To_End_Delay_Avg.awk  tr_escenario.tr
gawk -f ./awk/ok_delay/End_To_End_Delay_Inst.awk  tr_escenario.tr > End_To_End_Delay.data
xgraph -bb -t End_To_End_Delay -x "packet id" -y "End2End Delay (s)"  -bg white End_To_End_Delay.data -geometry 800x400
echo ""
echo "Jitter:"
gawk -f ./awk/ok_jitter/Jitter_Avg.awk  tr_escenario.tr
gawk -f ./awk/ok_jitter/Jitter_Inst.awk  tr_escenario.tr > Jitter.data
xgraph -bb -t Jitter -x "packet id" -y "Jitter (ms)"  -bg white Jitter.data -geometry 800x400
