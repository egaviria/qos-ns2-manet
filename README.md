# Monitoring of the parameters that generate quality of service (QoS).

## NS2 install (Network Simulator) and OLSR - ZRP patch 

NS2 is simply an event-driven simulation tool that has proved useful in studying the dynamic nature of communication networks. Simulation of wired as well as wireless network functions and protocols (e.g., routing algorithms, TCP, UDP) can be done using NS2. In general, NS2 provides users with a way of specifying such network protocols and simulating their corresponding behaviors.

### Compatibility

### UNIX
  - OS X
  - Linux
  - Solaris
### Windows
  - Cygwin


## Install dependencies and updates

Now you need to download some essential packages for ns2, these packages can be downloaded using the following commands::

```sh
$ sudo apt-get update
$ sudo apt-get gcc
$ sudo apt-get install build-essential autoconf automake
$ sudo apt-get install tcl8.5-dev tk8.5-dev
$ sudo apt-get install perl xgraph libxt-dev libx11-dev libxmu-dev
```

## NS2 install

Download ns-allinone-2.35_gcc482.tar move to ~ / extract or execute the following command:

```sh
$ tar xfz ns-allinone-2.35_gcc482.tar
```

Download the patches for OLSR (umolsr_for-zrp-patched_ns235), ZRP (zrp-ns235.patch) and copy to ~ / ns-allinone-2.35 /, run the following commands to apply the patches:

```sh
$ patch -p0 < zrp-ns235.patch
$ patch -p0 < umolsr_for-zrp-patched_ns235.patch
$ ./install
```

After completing the installation, open the bashrc file to set environment variables, type the following command:

```sh
$ sudo gedit ~/.bashrc
```

Now an editor window appears, copy and paste the following codes at the end of the text file (note that '/home/egaviria/ns-allinone-2.35/octl-1.14' on each line in the following code must be replaced with your location where the file 'ns-allinone-2.35.tar.gz' is extracted), you can use the whoami command to see the username

```sh
# LD_LIBRARY_PATH
OTCL_LIB=/home/egaviria/ns-allinone-2.35/otcl-1.14
NS2_LIB=/home/egaviria/ns-allinone-2.35/lib
X11_LIB=/usr/X11R6/lib
USR_LOCAL_LIB=/usr/local/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OTCL_LIB:$NS2_LIB:$X11_LIB:$USR_LOCAL_LIB
```

```sh
# TCL_LIBRARY
TCL_LIB=/home/egaviria/ns-allinone-2.35/tcl8.5.10/library
USR_LIB=/usr/lib
export TCL_LIBRARY=$TCL_LIB:$USR_LIB
```

```sh
# PATH
XGRAPH=/home/egaviria/ns-allinone-2.35/bin:/home/egaviria/ns-allinone-2.35/tcl8.5.10/unix:/home/egaviria/ns-allinone-2.35/tk8.5.10/unix
NS=/home/egaviria/ns-allinone-2.35/ns-2.35/
NAM=/home/egaviria/ns-allinone-2.35/nam-1.15/
PATH=$PATH:$XGRAPH:$NS:$NAM
```

save and close editor

Close terminal window and start a new terminal window and now change directory to ns-2.35 and validate ns-2.35 by following command (30-45 minute delay)

```sh
$ cd ns-2.35
$ ./validate
```

If the installation is successful, you will be able to see % at the command prompt while typing the following command

```sh
$ ns
```

# QOS NS2 MANET

module for evaluating the performance of a MANET network on the NS-2 tool by monitoring the parameters that generate quality of service (QoS).

  - PRD (Package delivery ratio)
  - Throughput
  - Delay
  - Jitter

## AWK filters!

high level programming language which is used to process trace files

  - Average
  - Instant (to graph)

### Install and run 

QOS Manet requires [ns2](https://www.isi.edu/nsnam/ns/) to run.

Install the dependencies and start the simulation.

```sh
$ bash ./qos.sh
```

And follow the instructions.

### Image

<img src="https://gitlab.com/egaviria/qos-ns2-manet/-/raw/master/images/qos.jpg" alt="QOS Image" width="400">


## Tools and languages

|  | Link |
| ------ | ------ |
| NS2 | [https://www.isi.edu/nsnam/ns/](https://www.isi.edu/nsnam/ns/) |
| Nam: Network Animator | [https://www.isi.edu/nsnam/nam/](https://www.isi.edu/nsnam/nam/) |
| XGRAPH | [http://www.xgraph.org/](http://www.xgraph.org/) |
| AWK | [https://www.grymoire.com/Unix/Awk.html](https://www.grymoire.com/Unix/Awk.html) |
| SHELL SCRIPTS | [http://research.iac.es/sieinvens/SINFIN/CursoUnix/cap8.php](http://research.iac.es/sieinvens/SINFIN/CursoUnix/cap8.php) |


## Development

Want to contribute? Contributions are always welcome!

## Todos

 - Improve Interface
 - Allow to configure other parameters

## Collaborators

 - Esteban Gaviria Restrepo
 - Angela Lizeth Muñoz Obonaga

License
----

MIT


**Free Software!**